#include <stdio.h>
#include <stdbool.h>

#define SUBGRID_HEIGHT 3
#define SUBGRID_WIDTH SUBGRID_HEIGHT
#define SUBGRIDS_IN_ROW 3
#define SUBGRIDS_IN_COLUMN 3
#define ROW_LENGTH (SUBGRIDS_IN_ROW * SUBGRID_WIDTH)
#define COLUMN_HIGH SUBGRID_HEIGHT * SUBGRIDS_IN_COLUMN
#define ROWS_COUNT SUBGRID_HEIGHT * SUBGRIDS_IN_COLUMN
#define COLUMN_COUNT SUBGRID_WIDTH * SUBGRIDS_IN_ROW
#define SUBMATRIX_COUNT SUBGRIDS_IN_COLUMN * SUBGRIDS_IN_ROW
#define NUMBER_ELEMENTS_IN_SUBMATRIX SUBGRID_HEIGHT * SUBGRID_WIDTH
#define END_OF_SUBMATRIX_ROW SUBGRID_WIDTH - 1
#define TESTCASES_COUNT 7

#ifdef DEBUG_BUILD
struct TestCase {
    const char *name; // Test case name, used for user to identify test case
    int grid[ROWS_COUNT][ROW_LENGTH]; // Filled sudoku grid
    bool allRowsValid;
    bool allColumnsValid;
    bool allSubgridsValid;
};
//@formatter:off
struct TestCase arrayOfTestCases[TESTCASES_COUNT] =
    {{"Correct fill",
    {{1,2,3,4,5,6,7,8,9},
    {4,5,6,7,8,9,1,2,3},
    {7,8,9,1,2,3,4,5,6},
    {9,1,2,3,4,5,6,7,8},
    {3,4,5,6,7,8,9,1,2},
    {6,7,8,9,1,2,3,4,5},
    {8,9,1,2,3,4,5,6,7},
    {2,3,4,5,6,7,8,9,1},
    {5,6,7,8,9,1,2,3,4}},
    true, true, true},

    {"Another correct fill",
    {{4,3,5,2,6,9,7,8,1},
    {6,8,2,5,7,1,4,9,3},
    {1,9,7,8,3,4,5,6,2},
    {8,2,6,1,9,5,3,4,7},
    {3,7,4,6,8,2,9,1,5},
    {9,5,1,7,4,3,6,2,8},
    {5,1,9,3,2,6,8,7,4},
    {2,4,8,9,5,7,1,3,6},
    {7,6,3,4,1,8,2,5,9},},
    true, true, true},

    {"Row error",
    {{4,3,2,2,6,9,7,8,1},
    {6,8,5,5,7,1,4,9,3},
    {1,9,7,8,3,4,5,6,2},
    {8,2,6,1,9,5,3,4,7},
    {3,7,4,6,8,2,9,1,5},
    {9,5,1,7,4,3,6,2,8},
    {5,1,9,3,2,6,8,7,4},
    {2,4,8,9,5,7,1,3,6},
    {7,6,3,4,1,8,2,5,9}},
    false, true, true},

    {"Column error",
    {{4,3,5,2,6,9,7,8,1},
    {6,8,2,5,7,1,4,9,3},
    {1,9,7,8,3,4,5,6,2},
    {8,2,6,1,9,5,3,4,7},
    {3,7,4,6,8,2,9,1,5},
    {1,5,9,7,4,3,6,2,8},
    {5,1,9,3,2,6,8,7,4},
    {2,4,8,9,5,7,1,3,6},
    {7,6,3,4,1,8,2,5,9}},
    true, false, true},

    {"Subgrid error",
    {{4,3,5,2,6,9,7,8,1},
    {6,8,2,5,7,1,4,9,3},
    {8,9,7,1,3,4,5,6,2},
    {1,2,6,8,9,5,3,4,7},
    {3,7,4,6,8,2,9,1,5},
    {9,5,1,7,4,3,6,2,8},
    {5,1,9,3,2,6,8,7,4},
    {2,4,8,9,5,7,1,3,6},
    {7,6,3,4,1,8,2,5,9}},
    true, true, false},

    {"Rubbish error",
    {{5,9,6,1,4,2,5,3,7},
    {6,1,4,3,5,8,2,4,8},
    {5,6,9,4,1,2,5,3,6},
    {1,9,5,3,6,8,4,1,6},
    {5,9,3,6,3,4,8,2,1},
    {5,9,5,3,2,1,4,5,6},
    {1,3,6,4,8,6,5,2,5},
    {4,1,2,3,6,8,4,9,2},
    {3,6,8,7,4,1,5,6,3}},
    false, false, false},

    {"Summing is not enough",
    {{1,3,3,4,5,6,7,7,9},
    {4,5,6,7,7,9,1,3,3},
    {7,7,9,1,3,3,4,5,6},
    {9,1,3,3,4,5,6,7,7},
    {3,4,5,6,7,7,9,1,3},
    {6,7,7,9,1,3,3,4,5},
    {7,9,1,3,3,4,5,6,7},
    {3,3,4,5,6,7,7,9,1},
    {5,6,7,7,9,1,3,3,4}},
    false, false, false}
    };

//@formatter:on

void printFailureReasonIfValuesNotEqual(const char *valueName, bool expected, bool actual) {
    if (expected != actual) {
        printf("\texpected %s %d, and actual %d\n", valueName, expected, actual);
    }
}
#endif

bool isDuplicatesInRow(int *row) {
    for (int i = 0; i < ROW_LENGTH; i++) {
        int m = row[i];
        for (int j = i + 1; j < ROW_LENGTH; j++) {
            if (m == row[j]) {
                return false;
            } else {

            }
        }
    }
    return true;
}

bool isAllRowsCorrect(int grid[ROWS_COUNT][ROW_LENGTH]) {
    for (int i = 0; i < ROWS_COUNT; ++i) {
        if (isDuplicatesInRow(&grid[i][0]) == false) {
            return false;
        } else {

        }
    }
    return true;
}

bool isDuplicatesInColumn(int *column) {
    for (int i = 0; i < ROWS_COUNT * ROW_LENGTH; i += ROW_LENGTH) {
        int m = column[i];
        for (int j = i + ROW_LENGTH; j < ROWS_COUNT * ROW_LENGTH; j += ROW_LENGTH) {
            if (m == column[j]) {
                return false;
            } else {

            }
        }
    }
    return true;
}

bool isAllColumnsCorrect(int grid[ROWS_COUNT][ROW_LENGTH]) {
    for (int i = 0; i < ROW_LENGTH; i++) {
        if (isDuplicatesInColumn(&grid[0][i]) == false) {
            return false;
        } else {

        }
    }
    return true;
}

struct DigitCoord {
    int row;
    int column;
};

struct DigitCoord digitToCompare;
struct DigitCoord digitToCompareWith;

void Debug_printDuplicateFound(int digit, int firstIndex, int duplicateIndex) {
#ifdef DEBUG_BUILD
    printf("Duplicate found: %d[%d] %d[%d]\n", digit, firstIndex, digit, duplicateIndex);
#else
    fprintf(stderr, "Duplicate found: %d[%d] %d[%d]\n", digit, firstIndex, digit, duplicateIndex);
#endif
}

bool hasDuplicatedDigit(int *subgridTopLeft, struct DigitCoord digitToCompare) {
    int elementOfSubgrid = 0;
    int elementToCompare = 0;
    for (; digitToCompareWith.row < SUBGRID_HEIGHT; digitToCompareWith.row++) {
        digitToCompareWith.column = digitToCompare.column + 1;
        if (digitToCompare.column == END_OF_SUBMATRIX_ROW || digitToCompare.row != digitToCompareWith.row) {
            digitToCompareWith.column = 0;
        } else {

        }
        elementToCompare = digitToCompareWith.row * ROW_LENGTH + digitToCompareWith.column;

        for (; digitToCompareWith.column < SUBGRID_WIDTH; digitToCompareWith.column++) {
            if (subgridTopLeft[elementOfSubgrid] == subgridTopLeft[elementToCompare]) {
                Debug_printDuplicateFound(subgridTopLeft[elementOfSubgrid], elementOfSubgrid, elementToCompare);
                return false;
            } else {

            }
            elementToCompare++;
        }
    }
    return true;
}

bool isDuplicatesInSubgrid(int *grid) {
    for (digitToCompare.row = 0; digitToCompare.row < SUBGRID_HEIGHT; digitToCompare.row++) {
        for (digitToCompare.column = 0; digitToCompare.column < SUBGRID_WIDTH; digitToCompare.column++) {
            digitToCompareWith.row = digitToCompare.row;
            if (digitToCompare.column == END_OF_SUBMATRIX_ROW) {
                digitToCompareWith.row++;
            } else {

            }

            if (hasDuplicatedDigit(grid, digitToCompare) == false) {
                return false;
            } else {

            }
        }
    }
    return true;
}

bool isAllSubgridCorrect(int grid[ROWS_COUNT][ROW_LENGTH]) {
    for (int row = 0; row < ROWS_COUNT; row += SUBGRID_HEIGHT) {
        for (int column = 0; column < COLUMN_COUNT; column += SUBGRID_WIDTH) {
            if (isDuplicatesInSubgrid(&grid[row][column]) == false) {
                return false;
            } else {

            }
        }
    }
    return true;
}

int main(int argc, char **argv) {
#ifdef DEBUG_BUILD
    for (int i = 0; i < TESTCASES_COUNT; ++i) {

        struct TestCase *expected = arrayOfTestCases + i;

        bool actualRowsValid = isAllRowsCorrect(expected->grid);
        bool actualColsValid = isAllColumnsCorrect(expected->grid);
        bool actualSubgridsValid = isAllSubgridCorrect(expected->grid);

        if (expected->allRowsValid == actualRowsValid && expected->allColumnsValid == actualColsValid
                && expected->allSubgridsValid == actualSubgridsValid) {
            printf("[PASSED] TestCase %s\n", expected->name);
        } else {
            printf("[FAILED] TestCase %s because:\n", expected->name);
            printFailureReasonIfValuesNotEqual("row valid", expected->allRowsValid, actualRowsValid);
            printFailureReasonIfValuesNotEqual("col valid", expected->allColumnsValid, actualColsValid);
            printFailureReasonIfValuesNotEqual("subgrid valid", expected->allSubgridsValid, actualSubgridsValid);
        };
    }
#else
    // fill grid
    for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                scanf("%d", &sudokuGrid[i][j]);
            }
        }
        if (isAllRowsCorrect(sudokuGrid) && isAllColumnsCorrect(sudokuGrid) && isAllSubgridCorrect(sudokuGrid)) {
            printf("true\n");
        } else {
            printf("false\n");
        };
#endif
}

